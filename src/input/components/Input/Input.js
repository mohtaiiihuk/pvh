import React from 'react';

import styles from './styles.css';

export default ({ input, meta }) => (
  <div>
    <input {...input} />
    {meta.touched && meta.error && <div className="error">{meta.error}</div>}
  </div>
)