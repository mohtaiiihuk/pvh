import { compose } from 'redux';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
// Named imports, saves your bundle size ;)
import isEmail from 'validator/lib/isEmail';
import { addUser } from 'src/user/redux/actions';

import UserAddForm from './UserAddForm';

export default compose(
  connect(
    null,
    {
      onSubmit: addUser,
    },
  ),
  reduxForm({
    form: 'UserAdd',
    validate: (data) => {
      // I only add validation for email to save some time. Any other validation can be done the same way.
      const errors = {};

      if (data.email && !isEmail(data.email)) {
        errors.email = 'Email is wrong.';
      }

      return errors;
    },
  }),
)(UserAddForm);
