import React, { Component } from 'react';
import { Field, FieldArray } from 'redux-form';
import AddressForm from 'src/user/components/AddressForm';
import Input from 'src/input/components/Input';

import styles from './styles.css';

export default class UserAddForm extends Component {
  render() {
    const { handleSubmit, valid, fields } = this.props;

    return (
      <form className="user-form" onSubmit={handleSubmit}>
        <Field
          name="name"
          placeholder="name"
          type="text"
          required
          component="input"
        />
        {/* I only use the error displaying for one field, try to save more time for coding */}
        <Field
          name="email"
          placeholder="email"
          type="email"
          required
          component={Input}
        />
        <Field
          name="phone"
          placeholder="phone"
          type="phone"
          required
          component="input"
        />
        <Field
          name="gender"
          required
          component="select"
        >
          <option value="Male">Male</option>
          <option value="Female">Female</option>
        </Field>
        <FieldArray className="address-wrap" name="address" component={AddressForm} />
        <button type="submit" disabled={!valid}>Add User</button>
      </form>
    )
  }
}