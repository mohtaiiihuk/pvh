import React, { Component } from 'react';
import UserAddForm from 'src/user/components/UserAddForm';
import UsersListItem from 'src/user/components/UsersListItem';

import styles from './styles.css';

export default class UsersList extends Component {
  render() {
    const { users, deleteUser } = this.props;

    return (
      <div className="grid">
        <div className="user-form-container">
          <h1>Add user form</h1>
          <UserAddForm />
        </div>
        {users.map((user, idx) => <UsersListItem key={idx} user={user} deleteUser={deleteUser} />)}
      </div>
    )
  }
}