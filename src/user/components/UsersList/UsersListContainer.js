import { connect } from 'react-redux';
import UsersList from './UsersList';
import { deleteUser } from 'src/user/redux/actions';

export default connect(
  state => state.user,
  {
    deleteUser,
  },
)(UsersList);