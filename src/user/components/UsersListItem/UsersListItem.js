import React from 'react';

import styles from './styles.css';

export default ({ user, deleteUser }) => (
  <div className="user-item">
    <span className="labelSpan">{user.name}</span>
    <span className="labelSpan">{user.email}</span>
    <button onClick={() => deleteUser(user.name)}>delete</button>
  </div>
)