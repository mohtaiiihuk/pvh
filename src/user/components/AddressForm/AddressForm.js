import React from 'react';
import { Field } from 'redux-form';

export default ({ fields }) => (
  <div>
    <div>
      <button type="button" onClick={() => fields.push({})}>Add Address</button>
    </div>
    {fields.map((address, index) =>
      <div key={index}>
        <button
          type="button"
          title="Remove Member"
          onClick={() => fields.remove(index)}
        >
          Remove
        </button>
        <h4>Address #{index + 1}</h4>
        <Field
          name={`${address}.number`}
          type="number"
          component="input"
          placeholder="Number"
        />
        <Field
          name={`${address}.street`}
          type="text"
          component="input"
          placeholder="Street"
        />
        <Field
          name={`${address}.city`}
          type="text"
          component="input"
          placeholder="City"
        />
        <Field
          name={`${address}.zipcode`}
          type="number"
          component="input"
          placeholder="Zip Code"
        />
      </div>
    )}
  </div>
)