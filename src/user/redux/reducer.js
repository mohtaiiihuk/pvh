import { createReducer } from 'src/utils';
import { ADD_USER, DELETE_USER } from 'src/user/redux/actions';

export default createReducer({ users: [{ name: 'Ven', email: 'koroleven@yandex.ru' }] }, {
  [ADD_USER]: (state, { user }) => ({ ...state, users: [ ...state.users, user ] }),
  // we should use a unique ID for getting user, but we don't have any( I could user math.random but it doesn't give us a unique number every time),
  // use name for now.
  [DELETE_USER]: (state, { userName }) => ({ ...state, users: state.users.filter(u => u.name !== userName) }),
});
