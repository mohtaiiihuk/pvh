import React, { Component } from 'react';
import { configureStore } from 'src/store';
import { Provider } from 'react-redux';
import UsersList from 'src/user/components/UsersList';

import logo from './logo.svg';
import './styles.css';

const store = configureStore();

// for debuggin, remove from production, or use logger/redux-devtool
window.store = store;

export default () => (
  <Provider store={store}>
    <UsersList />
  </Provider>
);
