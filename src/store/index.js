import { createStore } from 'redux';
import rootReducer from 'src/store/reducers';

// Here goes all the setup for the store

export const configureStore = () => createStore(rootReducer);
