# User add form test assignment

I used create-react-app as a boilerplate.

## How to run

1. `npm install && npm start`


## What I did and what I didn't

### What I did
1. I did folder structure
2. Project set up
3. Redux
4. Composition

### What I didn't do
1. Too much styling(I usuallt use csjss), styling architecture(classNames, selectors)
2. Full Validation
3. Tests


### What I would do

1. I would like to spend more time on styling.
2. I would save temporary state in localStorage and use it as a defaultState for the user's reducer.
3. I would write some small helpers for the forms. Validation/building forms look almost the same.
